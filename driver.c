#include <stdalign.h>
#include <stdio.h>
#include <stdint.h>

extern size_t strlen_baseline(const char * ptr);

int main()
{
    alignas(16) char str[] = "Hello123";
    char const *ptr = str;
    printf("strlen_baseline: %#04lx\n",
           strlen_baseline(str));
    printf("strlen_baseline: %#04lx\n",
           strlen_baseline(ptr));

    return 0;
}
